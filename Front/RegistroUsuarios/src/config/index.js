export default {
    region: 'us-west-2',
    IdentityPoolId: 'us-west-2_hFinwHJ2r',
    UserPoolId: 'eu-west-1:12345678910',
    ClientId: '7dnbupon0k4vq0r9gnt3ibn25n',
    // This will be the upload endpoint that we got from the last tutorial
    s3SignedUrl: 'https://rvv1a9to8j.execute-api.eu-west-1.amazonaws.com/dev/upload-node'
  }